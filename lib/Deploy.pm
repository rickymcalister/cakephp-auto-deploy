#! /usr/bin/perl
#
# @description Deploy module
# @author ricky mcalister ( ricky.mcalister@boxprojex.com )
# @date 2012-08-25
#
# @usage 
# use lib ('lib');
# use Deploy;
#
# ##
package Deploy;
use strict;
use warnings;
use POSIX 'strftime';
use Net::OpenSSH;
use Exporter;

our @ISA= qw( Exporter );

# sub routines that can be exported.
our @EXPORT_OK = qw( deploy );

# sub routines exported by default.
our @EXPORT = qw( deploy );

#
# deploy sub routine
# 
# @param ( hash ) $settings, ( array ) @app_symlinks
# @return ( void )
#
sub deploy {
  my $settings = shift;
  my @app_symlinks = @_;

  # generate a release directory name
  my $now = strftime '%Y%m%d%H%M%S', localtime;

  print "start : project deployment initiated\n\r";

  #
  # establish ssh connection
  #

  # establish a new ssh connection with the remote machine
  my $ssh = Net::OpenSSH->new($settings->{'user'}.'@'.$settings->{'host'});
  $ssh->error and
    die "error : ssh connection could not be established : " . $ssh->error;

  #
  # clone project repo and submodules
  #

  print "\n\rcreate : cloning repository from $settings->{'repository'} to new release $settings->{'deploy_to'}/$settings->{'releases_dir'}/$now \n\r";

  # execute git clone on the remote machine
  $ssh->system("git clone $settings->{'repository'} $settings->{'deploy_to'}/$settings->{'releases_dir'}/$now") or
    die "error : repository could not be cloned : " . $ssh->error;

  # check if have submodules been enabled
  if ($settings->{'use_submodules'}) {
    print "create : updating git submodules \n\r";

    # update git submodules
    $ssh->system("cd $settings->{'deploy_to'}/$settings->{'releases_dir'}/$now && git submodule update --init") or
      die "error : submodules could not be updated : " . $ssh->error;
  }

  #
  # create app specific symlinks
  #

  print "\n\rcreate : set up app specific symlinks\n\r";

  # set up app symlinks
  foreach my $app_symlink (@app_symlinks) {
    print "create : app symlink : $app_symlink \n";

    $ssh->system("ln -s $settings->{'deploy_to'}/$settings->{'shared_dir'}/$app_symlink $settings->{'deploy_to'}/$settings->{'releases_dir'}/$now/$app_symlink") or 
      die "error : symlink could not be created for $app_symlink : " . $ssh->error;
  }

  #
  # reset webroot symlink
  #

  print "\n\rreset : current symlink\n\r";

  # reset the current symlink
  $ssh->system("rm -rf $settings->{'deploy_to'}/$settings->{'current_dir'}") or
    die "error : current release symlink could not be reset : " . $ssh->error;

  # create the new current release symlink
  $ssh->system("ln -s $settings->{'deploy_to'}/$settings->{'releases_dir'}/$now $settings->{'deploy_to'}/$settings->{'current_dir'}") or
    die "error : current release symlink could not be created : " . $ssh->error;

  #
  # clean up
  #

  # capture the list of releases
  my($out, $err) = $ssh->capture2("find $settings->{'deploy_to'}/$settings->{'releases_dir'} -mindepth 1 -maxdepth 1 -type d");
  my @releases = split(/\n/, $out);

  # sort the existing releases so that the most recent releases are first in the array
  @releases = sort { lc($b) cmp lc($a) } @releases;

  # count the total number of existing releases
  my $num_releases = scalar @releases;

  # delete old releases if the total number of releases is greater than 
  # the number of releases to be kept ( $keep_releases )
  if ($num_releases > $settings->{'keep_releases'}) {
    print "\n\rremove : cleaning up old releases\n\r";

    for (my $i = $settings->{'keep_releases'}; $i < $num_releases; $i++) {
      print "remove : $releases[$i]\n";
      $ssh->system("rm -rf $releases[$i]");
    }
  }

  print "\n\rend : deployment process complete\n\rdone\n\r";
}

# return true
1;