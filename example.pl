#! /usr/bin/perl
#
# @description Automated Dynamic Deployment for CakePHP 2.x
# @author ricky mcalister ( ricky.mcalister@boxprojex.com )
# @date 2012-08-25
# @version v0.2
#
# @dependencies perl 5, Net::OpenSSH, POSIX
#
# @install 
# $ sudo apt-get install cpanminus
# $ sudo cpanm Net::OpenSSH
#
# @usage 
# $ ./script.pl deploy
#
use strict;
use warnings;
use FindBin;
use lib $FindBin::Bin . '/lib';
use Deploy;

# use constants to define true / false booleans
use constant false => 0;
use constant true  => 1;

# deploy script settings
my $settings = {
  user => 'remote_user_name',
  host => 'remote_host_name',
  repository => 'git_repository',
  deploy_to => '/path/to/root/directory/on/remote/server',
  releases_dir => 'releases',
  shared_dir => 'shared',
  current_dir => 'current',
  keep_releases => 3,
  use_submodules => true
};

# app specific symlinks
my @app_symlinks = ("Config/database.php", "webroot/index.php", "tmp");

# inspect the arguements that have been passed to the script
if ($#ARGV >= 0) {
  if ($ARGV[0] eq 'deploy') {
    # execute the deploy sub routine
    deploy($settings, @app_symlinks);
  }
} else {
  print "error : specify the task to be executed e.g. deploy\n\r";
  die "abort : not enough arguments";
}