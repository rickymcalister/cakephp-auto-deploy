# Automated Dynamic Deployment for CakePHP 2.x

Designed to automate deployment tasks for CakePHP 2.x applications using Git repositories via SSH.

_This software is in the very early stages of development and has not been properly tested. It is recommended that this software is not used in a production environment. Use at your own risk._

## Version

Version 0.2

## Dependencies

There are a number of dependencies required in order to successfully execute this deployment script.

- Perl 5
- Perl Modules ( Net::OpenSSH, POSIX )
- CakePHP 2.0+
- Git Installed on the Remote Server

## Installation

*Download or clone this repository.*

*Install the required dependencies.*

Linux Ubuntu

	$ sudo apt-get install cpanminus
	$ sudo cpanm Net::OpenSSH

For further Perl module installation instructions and details for installing Perl modules on other operating systems see here [http://www.cpan.org/modules/INSTALL.html](http://www.cpan.org/modules/INSTALL.html)

*Set Up Deployment Configuration Script*

Use example.pl as a template to copy and create as many deployment scripts as required e.g. dev.pl, staging.pl, production.pl, etc. Modify the contents of the deployment scripts, defining the settings and app_symlinks configuration variables.

	my $settings = {
		user => 'remote_user_name',
		host => 'remote_host_name',
		repository => 'git_repository',
		deploy_to => '/path/to/root/directory/on/remote/server',
		releases_dir => 'releases',
		shared_dir => 'shared',
		current_dir => 'current',
		keep_releases => 3,
		use_submodules => true
	};

	my @app_symlinks = ("Config/database.php", "webroot/index.php", "tmp"); 

The remote user should have sufficient privileges to create files / directories and remove files and directories on the remote server.

Set the executable bit for each of the deployment scripts.

	$ sudo chmod 755 dev.pl

## Set Up Remote Server

The remote server should look something like the following ...

	/root
		/releases
		/shared
			/Config
				/database.php
			/webroot
				/index.php
			/tmp
				/cache
					/models
					/persistent
					/views
				/logs

## Usage

e.g.

	$ ./dev.pl deploy

